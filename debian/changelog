openvr (1.23.7~ds1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.23.7.
  * Add an upstream patch to fix the build.
  * Rebase upstream-pull-request-1342.patch to point to a new file location.
  * debian/clean: Clean bin/.

 -- Safir Secerovic <sapphire@linux.org.ba>  Sat, 11 Feb 2023 17:42:28 -0600

openvr (1.12.5~ds1-1) unstable; urgency=medium

  * New upstream release.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 07 Aug 2020 16:59:29 +0800

openvr (1.10.30~ds1-1) unstable; urgency=medium

  * New upstream release.
  * Replace previous patchset with patchset from upstream pull request.
  * Updated findjsoncpp.patch to find the jsoncpp lib in correctly
    location.
  * Add upstream pull request 1342 that fixed ftbfs.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Wed, 01 Apr 2020 08:04:04 +0800

openvr (1.8.19~ds1-3) unstable; urgency=medium

  * debian/control: fix overwriting files from libopenvr-api-dev.
    (Closes: #946424)

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 13 Dec 2019 15:52:18 +0800

openvr (1.8.19~ds1-2) unstable; urgency=medium

  * Drop private-lib-path patch.
  * Add a patch to build lib with versioned soname.
  * Split the package to libopenvr-api1 and libopenvr-dev.
  * Improve the patch to install into arch specific dir.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Tue, 03 Dec 2019 14:42:40 +0800

openvr (1.8.19~ds1-1) unstable; urgency=medium

  * New upstream version 1.8.19.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Wed, 13 Nov 2019 11:56:52 +0800

openvr (1.7.15~ds1-1) unstable; urgency=medium

  * New upstream version 1.7.15.
  * Fix private lib path in .pc file.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 11 Oct 2019 15:48:20 +0800

openvr (1.6.10~ds1-2) unstable; urgency=medium

  * Really build as shared libs with system jsoncpp instead of static.
  * Apply patches to fix ftbfs and make lintian happy.
  * debian/control: added shlibs depends line as now we ship shared libs
    instead of static.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Mon, 19 Aug 2019 12:15:11 +0800

openvr (1.6.10~ds1-1) unstable; urgency=medium

  * New upstream release.
  * Apply patchset to use system jsoncpp instead of embedded one.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 16 Aug 2019 11:18:13 +0800

openvr (1.5.17~ds1-2) unstable; urgency=medium

  * Added references to the embedded json implementation.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Wed, 14 Aug 2019 05:30:40 +0800

openvr (1.5.17~ds1-1) unstable; urgency=medium

  * Initial Release. (Closes: #933579)

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Thu, 01 Aug 2019 09:32:32 +0800
